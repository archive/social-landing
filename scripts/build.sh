#!/usr/bin/env bash

set -e

rm -rf public/

mkdir -p static/build

cp -r node_modules/materialize-css/dist/* static/build/
cp -r node_modules/@fortawesome/fontawesome-free/css/all.min.css static/build/css/font-awesome.css
cp -r node_modules/@fortawesome/fontawesome-free/webfonts static/build/

cp static/src/* static/build

hugo -v --stepAnalysis --gc
